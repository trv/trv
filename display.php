<?php
/* GNUkebox -- a free software server for recording your listening habits

   Copyright (C) 2009, 2011 Free Software Foundation, Inc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

header('Content-type: text/html; charset=utf-8');
require_once('database.php');

?>
<!DOCTYPE html>
<html>
<head>
   <title>Trivia</title>
</head>
<body>
       <h1>Trivia!</h1>

       <ul>
       <li><a href="play.php">Play an existing game of trivia</a></li>
       <li><a href="questions.php">Add questions</a></li>
       <li><a href="teams.php">Add team</a></li>
       <li><a href="team-aliases.php">Add alias to existing team</a></li>
       <li><a href="add-game.php">Add a new game</a></li>
       <li><a href="game-questions.php">Add questions to existing game</a></li>
       </ul>

</body>
</html>
