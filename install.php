<?php

/* GNUkebox -- a free software server for recording your listening habits

   Copyright (C) 2009 Free Software Foundation, Inc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

require_once('adodb/adodb-exceptions.inc.php');
require_once('adodb/adodb.inc.php');

if (file_exists('config.php')) {
	die('A configuration file already exists. Please delete <i>config.php</i> if you wish to reinstall.');
}

if (isset($_POST['install'])) {

	//Get the database connection string
	$dbms = $_POST['dbms'];
	if ($dbms == 'sqlite') {
		$filename = urlencode($_POST['filename']);
		$connect_string = 'sqlite://' . $filename;
	} else {
		$connect_string = $dbms . '://' . $_POST['username'] . ':' . $_POST['password'] . '@' . $_POST['hostname'] . ':' . $_POST['port'] . '/' . $_POST['dbname'];
	}

	$adodb_connect_string = str_replace('pgsql:', 'postgres:', $connect_string);

	try {
		$adodb =& NewADOConnection($adodb_connect_string);
	} catch (Exception $e) {
		var_dump($e);
		adodb_backtrace($e->gettrace());
		die("Database connection failure\n");
	}

	//Create tables

	$stage_one_queries = array(
"CREATE TABLE `trivia_games` (

  `venueID` int(11) NOT NULL default '0',

  `gamedatetime` datetime NOT NULL default '0000-00-00 00:00:00',

  `gameID` int(11) NOT NULL auto_increment,

  PRIMARY KEY  (`gameID`)

);

CREATE TABLE `trivia_venues` (
  `venueName` varchar(100) NOT NULL default '',
  `venueURL` varchar(100) NOT NULL default '',

  `venueID` int(11) NOT NULL auto_increment,

  PRIMARY KEY  (`venueID`)

);

CREATE TABLE `trivia_questions` (
  `question` varchar(100) NOT NULL default '',
  `questionExtra` varchar(100) NOT NULL default '',
  `questionHint` varchar(100) NOT NULL default '',
  `questionAnswer` varchar(100) NOT NULL default '',

  `questionID` int(11) NOT NULL auto_increment,

  PRIMARY KEY  (`questionID`)

);

CREATE TABLE `trivia_teams` (
  `teamName` varchar(100) NOT NULL default '',
  `teamID` int(11) NOT NULL auto_increment,

  PRIMARY KEY  (`teamID`)

);

CREATE TABLE `trivia_teamaliases` (
  `venueID` int(11) NOT NULL default '0',
  `gameID` int(11) NOT NULL default '0',
  `teamAlias` varchar(100) NOT NULL default '',
  `aliasID` int(11) NOT NULL auto_increment,

  PRIMARY KEY  (`aliasID`)

);

CREATE TABLE `trivia_answers` (
  `gameID` int(11) NOT NULL default '0',
  `questionID` int(11) NOT NULL default '0',
  `teamID` int(11) NOT NULL default '0',
  `answerID` int(11) NOT NULL auto_increment,

  PRIMARY KEY  (`answerID`)

);

CREATE TABLE `trivia_gamequestions` (
  `gameID` int(11) NOT NULL default '0',
  `questionID` int(11) NOT NULL default '0',
  `roundID` int(11) NOT NULL default '0',
  `gqID` int(11) NOT NULL auto_increment,

  PRIMARY KEY  (`gqID`)

);"	);


	foreach ($stage_one_queries as $query) {
		try {
			$adodb->Execute($query);
		} catch (Exception $e) {
			die('Database Error: ' . $adodb->ErrorMsg());
		}
	}


	$adodb->Close();

	$install_path = dirname(__FILE__) . '/';

	//Write out the configuration
	$config = "<?php\n \$config_version = " . $version .";\n \$connect_string = '" . $connect_string . "';\n\$install_path = '" . $install_path . "';\n \$adodb_connect_string = '" . $adodb_connect_string . "'; ";

	$conf_file = fopen('config.php', 'w');
	$result = fwrite($conf_file, $config);
	fclose($conf_file);

	if (!$result) {
		$print_config = str_replace('<', '&lt;', $config);
		die('Unable to write to file \'<i>config.php</i>\'. Please create this file and copy the following in to it: <br /><pre>' . $print_config . '</pre>');
	}

	die('Configuration completed successfully!');
}

?>
<html>
	<head>
		<title>GNUkebox Installer</title>
		<script type='text/javascript'>
			function showSqlite() {
				document.getElementById("sqlite").style.visibility = "visible";
				document.getElementById("networkdbms").style.visibility = "hidden";
			}

			function showNetworkDBMS() {
				document.getElementById("sqlite").style.visibility = "hidden";
				document.getElementById("networkdbms").style.visibility = "visible";
			}
		</script>
	</head>

	<body onload="showSqlite()">
		<h1>GNUkebox Installer</h1>
		<form method="post">
			<h2>Database</h2>
			Database Management System: <br />
			<input type="radio" name="dbms" value="sqlite" onclick='showSqlite()' checked>SQLite (use an absolute path)</input><br />
			<input type="radio" name="dbms" value="mysql" onclick='showNetworkDBMS()'>MySQL</input><br />
			<input type="radio" name="dbms" value="pgsql" onclick='showNetworkDBMS()'>PostgreSQL</input><br />
			<br />
			<div id="sqlite">
				Filename: <input type="text" name="filename" /><br />
			</div>
			<div id="networkdbms">
				Hostname: <input type="text" name="hostname" /><br />
				Port: <input type="text" name="port" /><br />
				Database: <input type="text" name="dbname" /><br />
				Username: <input type="text" name="username" /><br />
				Password: <input type="password" name="password" /><br />
			</div>
			<br />

			<br />
			<input type="submit" value="Install" name="install" />
		</form>
		<br />
		<div align="center"><a href="http://docs.jurg.no/gnufm_install.txt">Help</a></div>
	</body>
</html>


